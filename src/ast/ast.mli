open Exp

type uint = int

type num = int

type sort = Sid of int | Bool of bool | Bitvec of num | Array of sort * sort

(* hexadecimal, change to hex*)

type node =
    Nid of int
  | Input of sort 
  | State of sort 
  | Init of sort * node * node
  | Next of sort * node * node
  (*Expression*)
  | Uop of sort * uop * node
  | Bop of sort * bop * node * node
  | Top of sort * top * node * node * node
  | Idx of sort * opidx * node * uint * uint option
  (*Constants*)
  | One of sort
  | Ones of sort
  | Zero of sort
  | Constbin of sort * bool
  | Constdec of sort * uint (* unsigned integer *)
  | Consthex of sort * uint
  (*Property*)
  | Bad of node 
  | Constraint of node 
  | Output of node

type pnode = Node of int * node * string option | Sort of int * sort

type btor = Btor2 of pnode list option

(* Pretty Printing *)                                                                                                                                                                                
val pp_string :
  Format.formatter -> (unit, Format.formatter, unit) format -> unit -> unit
val pp_nl : Format.formatter -> unit -> unit
val pp_bool : Format.formatter -> bool -> unit
val pp_int : Format.formatter -> int -> unit
val pp_str : Format.formatter -> string -> unit
val pp_opt_str : Format.formatter -> string option -> unit
val pp_opt_uint : Format.formatter -> int option -> unit
val pp_num : Format.formatter -> num -> unit
val pp_sort : Format.formatter -> sort -> unit
val pp_uop : Format.formatter -> uop -> unit
val pp_bop : Format.formatter -> bop -> unit
val pp_top : Format.formatter -> top -> unit
val pp_idx : Format.formatter -> opidx -> unit
val pp_node : Format.formatter -> node -> unit
val pp_pnode : Format.formatter -> pnode -> unit
val pp_nodes : Format.formatter -> pnode list -> string
val pp_opt_nodes : Format.formatter -> pnode list option -> string
val pp_prog : Format.formatter -> btor -> string
