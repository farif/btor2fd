open Format
open Ast
open Parsing

(* 
    Btor2 overflow-example progrm: 
    a) sid = Sort 
    b) nid = Node 
*)
(*
let s1 = Bitvec 1
let s2 = Bitvec 8

let n1 = Sort (1, s1)

let n2 = Sort (2, s2)

let a1 = Sort (3, Array (s1, s2))

(* Exmaple Program evaluated with unrolled sort and nodes  *)
let n3 = Node(3,  Input(s1, Some "turn"))

let n4 = Node (4, Const (s2, Zero))

let n5 = Node (5, State (s2, Some "a"))

let n6 = Node (6, State (s2, Some "b"))

let n7 = Node (7, Init (s2, n5, n4))

let n8 = Node (8, Init (s2, n6, n4))

let n9 = Node (9, Const (s2, Constdec ( 3)))

let n10 = Node (10, Exp (s2, Bop (Add, n5, n9)))

let n11 = Node (11, Exp (s2, Bop (Add, n6, n9)))

let n12 = Node (12, Exp(s2, Top (Ite, n3, n5, n10)))

(* No reference id *)
let nn3 = Node (999, Exp(s2, Uop (Neg, n3)))

let n13 = Node (13, Exp(s2, Top (Ite, nn3, n6, n10)))

let n14 = Node (14, Next (s2, n5, n12))

let n15 = Node (15, Next (s2, n5, n13))

let n16 = Node (16, Const (s2, Constdec 2))

let n17 = Node (17, Exp (s1, Bop (Eq, n5, n16)))

let n18 = Node (18, Exp (s1, Bop (Eq, n6, n16)))

let n19 = Node (18, Exp (s1, Bop (Add, n17, n18)))

let n20 = Node(20, Property (Bad(n19)))

let b1_prog =
  Btor2 (Some
    [ n1;
      n2;
      n3;
      n4;
      n5;
      n6;
      n7;
      n8;
      n9;
      n10;
      n11;
      n12;
      n13;
      n14;
      n15;
      n16;
      n17;
      n18;
      n19;
      n20;
    ])

let print_prog = pp_prog std_formatter b1_prog

(* Same Program with sort and node Ids  *)
let s1 = Sid 1
let s2 = Sid 2

let n1 = Sort(1, s1)
let n2 = Sort(2, s2)

let n3 = Node (3, Input (Sid 2, Some "Turn"))

let n4 = Node (4, Const (Sid 2, Zero ))

let n5 = Node (5, State (Sid 2, Some "a"))

let n6 = Node (6, State (Sid 2, Some "b"))

let n7 = Node (7, Init (Sid 2, Nid 5, Nid 4))

let n8 = Node (8, Init (Sid 2, Nid 6, Nid 4))

let n9 = Node (9, Const (Sid 2, Constdec (3)))

let n10 = Node (10, Exp (Sid 2, Bop (Add, Nid 5, Nid 9)))

let n11 = Node (11, Exp (Sid 2, Bop (Add, Nid 6, Nid 9)))

let n12 = Node (12, Exp (Sid 2, Top (Ite, Nid 3, Nid 5, Nid 10)))

(* No reference id *)
let nn3 = Node (999,Exp (Sid 2, Uop (Neg, Nid 3)))

let n13 = Node (13, Exp (Sid 2, Top (Ite, nn3, Nid 6, Nid 10)))

let n14 = Node (14, Next (Sid 2, Nid 5, Nid 12))

let n15 = Node (15, Next (Sid 2, Nid 5, Nid 13))

let n16 = Node (16, Const (Sid 2, Constdec (2)))

let n17 = Node (17, Exp (Sid 1, Bop (Eq, Nid 5, Nid 16)))

let n18 = Node (18, Exp (Sid 1, Bop (Eq, Nid 6, Nid 16)))

let n19 = Node (18, Exp (Sid 1, Bop (Add, Nid 17, Nid 18)))

let n20 = Node(20, Property ( Bad(Nid 19)))

let a2 = Sort (3, Array (Sid 1, Sid 2))
let btor_b1 = Btor2 None

let btor_b2 =  
  Btor2 (Some
    [ n1;
      n2;
      n3;
      n4;
      n5;
      n6;
      n7;
      n8;
      n9;
      n10;
      n11;
      n12;
      n13;
      n14;
      n15;
      n16;
      n17;
      n18;
      n19;
      n20;
    ])

let () = fprintf std_formatter "========Non Reference Program ==========\n"

let print_program = pp_prog std_formatter btor_b2

let () = fprintf std_formatter "========Parsed Program========\n"

let p = " 
        1 sort bitvec 5
        2 sort bitvec 10
        3 sort array 1 2 
        2 zero 1
        3 state 1 turn
        4 init 1 3 2
        5 one 1
        6 add 1 3 5
        7 next 1 3 6
        8 ones 1
        10 eq 9 3 8
        11 bad 10  
        "
(*let n = Parser.main Lexer.token (Lexing.from_string p) 
let () = (print_program n) std_formatter ()
*)
let _ =
  let lexbuf = Lexing.from_string p in
  let b = Parser.main Lexer.token lexbuf in
          (pp_prog std_formatter b)

let () = fprintf std_formatter "\n"
*)
let parse_with_error lexbuf =
  try Parser.main Lexer.token lexbuf with
  | Lexer.SyntaxError msg ->
    fprintf std_formatter "%a: %s\n" Lexer.print_position lexbuf msg; Btor2 None
  | Parser.Error ->
    fprintf std_formatter "%a: syntax error\n" Lexer.print_position lexbuf;
    exit (-1)

    
let parse_and_print lexbuf =
  match parse_with_error lexbuf with
  | Btor2 value -> match value with
                  Some value ->
                    (pp_nodes std_formatter value)
                  | None -> "\n"

let () = Printf.printf "Input File: "
let file = read_line()
  
let loop filename () =
  let inx = open_in file in
  let lexbuf = Lexing.from_channel inx in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
  print_string (parse_and_print lexbuf);
  close_in inx

let () = loop file ()

let () = fprintf std_formatter "\n"
