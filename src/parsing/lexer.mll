{
  open Format
  open Lexing
  open Parser

  let get = Lexing.lexeme

  exception SyntaxError of string
  
  let error fmt = Printf.kprintf (fun msg -> raise (SyntaxError msg)) fmt

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = pos.pos_cnum;
                 pos_lnum = pos.pos_lnum + 1;
      }

  let print_position outx lexbuf =
    let pos = lexbuf.lex_curr_p in
    fprintf outx "%s:%d:%d" pos.pos_fname
      pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)
  
(*
  let keyword_table = Hashtbl.create 20
  let _ =
    List.iter (fun (kwd, tok) -> Hashtbl.add keyword_table kwd tok)
    ["sort", SORT;
		"bitvec", BITVEC;
		"array", ARRAY;
		
(*  "true", BOOL(True);
    "false", BOOL(False); *)

    "init", INIT;
		"next", NEXT;
		"state", STATE;

    "input", INPUT;
		"one", ONE;
		"zero", ZERO;
		"ones", ONES;

    "const", CONSTBIN(true);
		"constd", CONSTDEC(0);
		"consth", CONSTHEX(1);

		"bad", BAD;
		"constraint", CONSTRAINT;
		"output", OUTPUT]
  *)
}


(* Define helper regexes *)
let ws = ['\t' ' ' '\009' '\012']+
(*let dos_newline = "\013\010"*)
let newline = '\r' | '\n' | "\r\n"
(*let newline = ('\010' | '\013' | "\013\010")*)

let comment = ";"
(*
let digit = ['0'-'9']
let integer = ['-' '+']? digit+
*)

let nat = ['1'-'9']
let digit = ('-')? ['0'-'9']

let hexdigit = ['0'-'9'] | ['a'-'f' 'A'-'F']
let hex = (hexdigit)+

let bin = ('0'|'1')+

let alpha = ['a'-'z' 'A'-'Z' '.' '$' '-' ':']
let ident = (alpha)(alpha|digit|'_')* (* regex for identifier *)

rule token = parse
  | comment   { skip_comment lexbuf}
  | ws    { token lexbuf }  
  | newline { NL }
  | digit+ as n  { Num (int_of_string n) }

  (* Constants       

  | (bin as b)  { CONSTBIN (bool_of_string b)}
  | (dec as h)  {CONSTDEC (int_of_string h)} 
  *)

  | "const" {CONSTBIN}        (* Bitvector Constant *)
  | "constd" {CONSTDEC}      (* Bitvector Constant Decimal *)   
  | "consth" {CONSTHEX}      (* Bitvector Constant Hex *) 

(*  | (hex as h)  { print_string h; } *) 

  | "sort" { SORT }        (* Sort Type *) 
  | "bitvec" {BITVEC}    (* BitVector / Register *)
  | "array" {ARRAY}      (* Array / Memory *)

  | "state" {STATE}
  | "input" {INPUT}    
  | "init" {INIT}        (* Initialization *)
  | "next" {NEXT}        (* Successor *)

  | "output" {OUTPUT}

  (* Properties *)
  | "bad" {BAD}
  | "constraint" {CONSTRAINT}
(*| "fair" {FAIR}
  | "Output" {OUTPUT}
  | "Justice" {JUSTICE} *)

  | "one" {ONE}       (* ? *)
  | "ones" {ONES}     (* Bitvector bv1 *)
  | "zero" {ZERO}     (* Bitvector bv0 *)

  (* ------------------------- *)
  (* [opidx] Indexed operators *)
  (* ------------------------- *)
  | "uext" {UEXT}      (* Unsigned extension *)
  | "sext" {SEXT}      (* signed extension *)    
  | "slice" {SLICE}   (* extraction *)   

  (* -------------------------------- *)
  (* [op] Unary unindexed operators  *)
  (* -------------------------------- *)
  | "not" { NOT }       (* Bit-wise *)
  | "neg" { NEG }       (* arithmetic *)

  | "redand" {REDAND}  (* reduction *)
  | "redor" {REDOR}    (* reduction *)
  | "redxor" {REDXOR}  (* reduction *)

  (* -------------------------------- *)
  (* Binary unindexed operators / op  *)
  (* -------------------------------- *)
  | "iff" {IFF}           (* Boolean *)
  | "implies" {IMPLIES}   (* Boolean *)

  | "eq" {EQ}             (* Equality *)
  | "neq" {NEQ}           (* Dis-equality *)

  (* Unsigned/Signed inequality *)
  | "ugt" {UGT}
  | "sgt" {SGT}
  | "ugte" {UGTE}
  | "sgte" {SGTE}
  | "ult" {ULT}
  | "slt" {SLT}
  | "ulte" {ULTE}
  | "slte" {SLTE}

  (* Boolean *)
  | "and" {AND}
  | "nand" {NAND}
  | "nor" {NOR}
  | "or" {OR} 
  | "xnor" {XNOR}
  | "xor" {XOR}

  (* Rotate, Shift *)
  | "rol" {ROL}
  | "ror" {ROR}
  | "sll" {SLL}
  | "sra" {SRA}
  | "srl" {SRL}

  (* Arithmetic *)
  | "add" {ADD}
  | "mul" {MUL}
  | "udiv" {UDIV}
  | "sdiv" {SDIV}
  | "smod" {SMOD}
  | "urem" {UREM}
  | "srem" {SREM}
  | "sub" {SUB}

  (* Overflow *)
  | "uaddo" {UADDO}
  | "saddo" {SADDO}
  | "udivo" {UDIVO}
  | "sdivo" {SDIVO}
  | "umulo" {UMULO}
  | "smulo" {SMULO}
  | "usubo" {USUBO}
  | "ssubo" {SSUBO}

  (* Concatenation *)
  |"concat" {CONCAT}

  (* Array Read *)
  | "read" {READ}

  (* -------------------------- *)
  (* (opidx) Ternary Operators *)
  (* -------------------------- *)
  | "ite" {ITE}      (* Conditional *)
  | "write" {WRITE}  (* Array Write *)

  | ident as s { ID s }  
  | eof        { EOF }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }

 and skip_comment = parse
    newline    { next_line lexbuf; token lexbuf }
  | eof        { EOF }
  | _          { skip_comment lexbuf }
(* 
  
BTOR2 Lexer 

| SID (ARRAY or BITVEC)
| NID (INPUT or STATE)
| NID opidx SID NID UNIT
| NID op SID NID
| NID (INIT or NEXT) SID NID NID
| NID (BAD or CONSTRAINT or OUTPUT) NID {};

opidx:
| NID op SID NID

op:
| NID SID SID

special_char_replacements = {'$': '', '\\': '.', ':': '_c_'}

NL = '\n'

SN='N%s'

COM=';'

 *)